// Cube

let the = "The";
let cube = "cube";
let of = "of";
let is = "is";
let number = 2

var getCube = number ** 3

let totalcube = the + " " + cube + " " + of + " " + number + " " + is + " " + getCube;
console.log(totalcube);


// Address
let addrray = [444, 'Mahogany St.', 'Camella Homes', 'Greenheights', 'Paranaque'];

let add1 = addrray[0]
let add2 = addrray[1]
let add3 = addrray[2]
let add4 = addrray[3]
let add5 = addrray[4]


let variableAddress = `${add1} ${add2} ${add3} ${add4} ${add5}`;
console.log(variableAddress);


// Animal
let animal = {
	name: 'Millie',
	type: 'Dog',
	age: 2,
	weight: "13 kg",
	breed: 'Dalmatian'

}

function greet(object) {

	let {name} = object
	let {type} = object
	let {breed} = object
	let {age} = object
	let {weight} = object


	let sentences = `${name} is a ${breed} type of ${type}, she's already ${age} years old and weight about ${weight}.`
	console.log(sentences)
}

greet(animal)


// Array Numbers
let array = [1,2,3,4,5]

array.forEach( e => console.log(e))

let reduceNumbers = array.reduce((acc,num) => acc + num)

console.log(reduceNumbers)


// DOG

class Dog {

	constructor(name, age, breed) {

		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let millie = new Dog("Millie", 4, "Beagle")
let enna = new Dog("Enna", 2, "Husky")
let petra = new Dog("Petra", 8, "Dachshund")

console.log(millie)
console.log(enna)
console.log(petra)